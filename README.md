# One Million

visitor counting for edws

## Requirements
`pip install --upgrade opencv-python imutils dlib`

## Acknowledgements
Adapted from the following:
*  Adrian Rosebrock, *Multi-object tracking with dlib*, PyImageSearch, [https://www.pyimagesearch.com/2018/10/29/multi-object-tracking-with-dlib/](https://www.pyimagesearch.com/2018/10/29/multi-object-tracking-with-dlib/), accessed on 21 May 2020